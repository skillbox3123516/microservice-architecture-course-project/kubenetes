{{- define "app.deployment" }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .name }}-deployment
  namespace: {{ .namespace }}
  labels:
    app: {{ .name }}
spec:
  replicas: {{ .replicas }}
  selector:
    matchLabels:
      app: {{ .name }}
  template:
    metadata:
      labels:
        app: {{ .name }}
    spec:
      containers:
      - name: {{ .name }}
        image: {{ .container.image.name }}:{{ .container.image.version }}
        imagePullPolicy: Never
        ports:
        - containerPort: {{ .container.port }}
        resources:
            requests:
                memory: 256M
                cpu: 50m
            limits:
                memory: 512M
                cpu: 200m
{{- if .container.env }}
        env:
{{ toYaml .container.env | indent 8 }}
{{- end }}
{{- if .container.volumeMounts }}
      volumes:
        - name: {{ .container.volumeMounts.name }}
{{- end }}
{{- end }}

{{- define "app.service" }}
{{- if .service }}
apiVersion: v1
kind: Service
metadata:
  name: {{ .name }}-service
  namespace: {{ .namespace }}
spec:
  selector:
    app: {{ .name }}
  {{ if .service.type  }}type: {{ .service.type }}{{ end }}
  ports:
  - name:
    protocol: TCP
    port: {{ .service.port}}
    targetPort: {{ .service.targetPort }}
    {{ if .service.nodePort }}nodePort: {{ .service.nodePort }} {{ end }}
{{- end }}
{{- end }}